﻿namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IOrderRepository Order { get; }
        void Save();
    }
}