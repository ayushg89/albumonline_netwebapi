﻿using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderProduct.Tests
{
    public class TestController
    {
        public static RepositoryContext GetRepositoryContext(string dbName)
        {
            // Create options for DbContext instance
            var options = new DbContextOptionsBuilder<RepositoryContext>()
                .UseSqlServer(dbName)
                .Options;

            // Create instance of DbContext
            var dbContext = new RepositoryContext(options);        

            return dbContext;
        }
    }
}
