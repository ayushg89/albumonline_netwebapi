using Contracts;
using Entities.ExtendedModels;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using OrderPicturesWeb.Controllers;
using OrderProduct.Tests;
using Repository;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public class Tests
    {        
        IRepositoryWrapper _repositoryWrapper = new RepositoryWrapper(TestController.GetRepositoryContext("Server=NL-1NL5ST2\\SQLEXPRESS;Database=photoonline;Trusted_Connection=True;MultipleActiveResultSets=true"));
        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void TestGetOrderById_WithValidId()
        {
            var order = _repositoryWrapper.Order.GetOrderById(1);
            if (order != null)
            {              
                Assert.IsInstanceOf<OrderExtended>(order);
                Assert.AreEqual(256, order.OrderMinimumWidth);               
            }   

        }

        [Test]
        public void TestGetOrderById_WithInValidId()
        {
            var order = _repositoryWrapper.Order.GetOrderById(8);
            if (order == null)
            {
                Assert.IsInstanceOf<OrderExtended>(order);
                Assert.AreEqual(null, order.OrderId);
            }

        }
        public List<NewOrderRequestObj> CreateOrderRequestValidObject()
        {
            return new List<NewOrderRequestObj>()
            {

                new NewOrderRequestObj
                {
                    ProductId = 1,
                    ProductQuantity = 2
                },
                new NewOrderRequestObj
                {
                    ProductId = 2,
                    ProductQuantity = 3
                },
                new NewOrderRequestObj
                {
                    ProductId = 3,
                    ProductQuantity = 5
                }
            };
        }
        [Test]
        public void TestCreateOrder_WithValidRequest()
        {
            var minimumBinWidth = _repositoryWrapper.Order.CreatOrder(CreateOrderRequestValidObject());
            if (minimumBinWidth !=0)
            {
                Assert.IsInstanceOf<decimal>(minimumBinWidth);
                Assert.AreEqual(148, minimumBinWidth);
            }

        }

        

        public List<NewOrderRequestObj> CreateOrderRequestZeroQuantityObject()
        {
            return new List<NewOrderRequestObj>()
            {

                new NewOrderRequestObj
                {
                    ProductId = 1,
                    ProductQuantity = 0
                },
                new NewOrderRequestObj
                {
                    ProductId = 2,
                    ProductQuantity = 3
                },
                new NewOrderRequestObj
                {
                    ProductId = 3,
                    ProductQuantity = 5
                }
            };
        }
        [Test]
        public void TestCreateOrder_WithZeroQuantity()
        {
            var minimumBinWidth = _repositoryWrapper.Order.CreatOrder(CreateOrderRequestZeroQuantityObject());
            if (minimumBinWidth == 0)
            {
                Assert.IsInstanceOf<decimal>(minimumBinWidth);
                Assert.AreEqual(0, minimumBinWidth);
            }           

        }

        public List<NewOrderRequestObj> CreateOrderRequest_WithInvalidProductIdObject()
        {
            return new List<NewOrderRequestObj>()
            {

                new NewOrderRequestObj
                {
                    ProductId = 100,
                    ProductQuantity = 1
                },
                new NewOrderRequestObj
                {
                    ProductId = 2,
                    ProductQuantity = 3
                },
                new NewOrderRequestObj
                {
                    ProductId = 3,
                    ProductQuantity = 5
                }
            };
        }
        [Test]
        public void TestCreateOrder_WithInvalidProductId()
        {
            var minimumBinWidth = _repositoryWrapper.Order.CreatOrder(CreateOrderRequest_WithInvalidProductIdObject());
            if (minimumBinWidth == 0)
            {
                Assert.IsInstanceOf<decimal>(minimumBinWidth);
                Assert.AreEqual(0, minimumBinWidth);
            }
            else
            {
                Assert.Fail();
            }

        }
    }
}